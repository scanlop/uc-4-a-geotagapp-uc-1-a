
    window.onload = () => {
        let places = staticLoadPlaces();
        renderPlaces(places);
    }

    function staticLoadPlaces() {
      conosle.log('places');
      return [
        {
          name: 'Location 1',
          location:
          {
            // decomment the following and add coordinates:
            lat: 52.2744113,
            lng: -7.11406,
          },
        },];
    } // End of staticLoadPlaces function

    var models = [
      {
        url: '../GeoMarker/marker.obj',
        scale: '0.5 0.5 0.5',
        info: 'Outside of range',
        rotation: '0 -10 30',
      },
      {
        url: '../GeoMarker/marker.obj',
        scale: '0.5 0.5 0.5',
        rotation: '0 -10 30',
        info: 'Inside range',
      },];

    var modelIndex = 0; // Red - outside of range
    var setModel = function (model, entity) {
      console.log('setModel');
      if (model.scale) {
        entity.setAttribute('scale', model.scale);
      }

      if (model.rotation) {
        entity.setAttribute('rotation', model.rotation);
      }

      if (model.position) {
        entity.setAttribute('position', model.position);
      }

      entity.setAttribute('gltf-model', model.url);

    };

    function renderPlaces(places) {
      console.log('render places');
      let scene = document.querySelector('a-scene');

      places.forEach((place) => {
        let latitude = place.location.lat;
        let longitude = place.location.lng;

        let model = document.createElement('a-entity');
        model.setAttribute('gps-entity-place', 'latitude: 52.2744113; longitude: -7.11406;');

        setModel(models[modelIndex], model);

        scene.appendChild(model);
      });
    } // End of renderPlaces method

    function acquireDistance() {

      let distance = document.querySelector('[gps-entity-place]').getAttribute('distance');
      let entity = document.querySelector('[gps-entity-place]');

      if (distance > 5) {
        setModel(models[0], entity);
      } // End of if we are outside range

      else {
        setModel(models[1], entity);  // Green model
      } // End of we are within the 5 metre range

    } // End of acquireDistance function