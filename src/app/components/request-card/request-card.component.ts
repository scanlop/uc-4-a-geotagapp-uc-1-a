import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-request-card',
  templateUrl: './request-card.component.html',
  styleUrls: ['./request-card.component.scss'],
})
export class RequestCardComponent implements OnInit {
  @Input() title: string;
  @Input() status: string;
  @Input() description: string;
  constructor() { }

  ngOnInit() { }

}
