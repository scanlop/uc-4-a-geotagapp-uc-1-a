import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const API_STORAGE_KEY = 'Agrisnap-key';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }

  /**
   * Get Local Data already stored
   * @param "key"
   */
  public getLocalData(key: string) {
    return this.storage.get(`${API_STORAGE_KEY}-${key}`);
  }
  /**
   * Set local data
   * @param "key"
   * @param "data"
   */
  public setLocalData(key: string, data: any) {
    return this.storage.set(`${API_STORAGE_KEY}-${key}`, data);
  }

  public removeLocalData(key: string) {
    return this.storage.remove(`${API_STORAGE_KEY}-${key}`);
  }
}
