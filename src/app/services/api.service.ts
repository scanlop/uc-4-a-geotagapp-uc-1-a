import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { getRequestsUrl,  postResponseUrl } from '../../../config'
import { PhotoService } from './photo.service';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public requests = Array<IRequest>();
  public contexts = [] as IRequestContext[];
  public referencePoint = {} as IReferencePoint;
  public geoCoordinates = {} as IGeoCoordinates;
  public selectedParcel = {} as IParcel;
  public random: string;
  public UC4A = {} as any;

  constructor(private http: HTTP,
              private photoService: PhotoService) { }

  getRequests(token: string) {
    const params = {
      agrisnapUid: token
    };

    const header = {};

    return this.http.get(getRequestsUrl, params, header);
  }

  /* getContexts(hash: string, token: string) {
    const url = getContextsUrl + hash + contexts;
    const params = {};
    const header = {
      app_id: requestAppId,
      app_key: requestAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(url, params, header);
  } */

  /* getReferencePoint(landParcelLabel: string, token: string) {
    const url = referencePointuUrl + landParcelLabel + referencePoint;
    const params = {};
    const header = {
      app_id: landAppId,
      app_key: landAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(url, params, header);
  } */

  /* getGeometry(landParcelLabel: string, token: string) {
    const url = geometryUrl + landParcelLabel + geometry;
    const params = {};
    const header = {
      app_id: landAppId,
      app_key: landAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(url, params, header);
  } */

  /* postLandLocations(token: string, latitude: number, longitude: number) {
    const body = {
      radius: 1000,
      shape: {
        type: 'Point',
        typeName: 'Point',
        coordinates: [longitude, latitude],
      },
    };
    const header = {
      app_id: landAppId,
      app_key: landAppKey,
      Authorization: bearer + token,
    };
    this.http.setDataSerializer('json');
    return this.http.post(postLandLocationsUrl, body, header);
  } */

  async postResponse(
    hash: string,
    name: string,
    data: string,
    image?: any,
  )
  {
    let base64: any;
    if(image instanceof Blob){
      const parsed = JSON.parse(data);
      base64 = parsed.base64
    } else {
      base64 = image;
    }
    // let url = '';
    // if (requestReference === 'REAP' || requestReference === 'undefined' || requestReference === 'FES' || requestReference === 'AECM') {
    //   // url = postResponseUrl
    //   // url = postResponsev1
    // }
    // else {
    //   // url = postResponseUrl + '/' + requestReference;
    //   url = postResponseUrl
    // }

/*     const formData = new FormData();
    // append file to formdata
    formData.append('file', image, name);
    formData.append('data', data);
    // set to multipart to support files
    this.http.setDataSerializer('multipart'); */
    this.http.setDataSerializer('json');
    const header = {
      // 'Content-Type': 'application/json'
      // app_id: requestAppId,
      // app_key: requestAppKey,
      // Authorization: bearer + token,
    };

    const formData = {
      // id,
      hash,
      // environment: 'testEnvironment',
      metadata: data,
      attachment: base64
    };
    // console.log('sending ', image);
    // console.log('sending ', JSON.stringify(formData));
    return this.http.post(postResponseUrl, formData, header);
  }

  randomGenerator(){
    this.random = 'X' + Math.floor(100000 + Math.random() * 900000);
  }
}

interface IRequest {
  id?: number;
  type?: string;
  geotag?: string;
  herdNumber?: string;
  year?: number;
  correspondenceId?: number;
  correspondenceDocNo?: number;
  hash?: string;
  status?: string;
  owner?: string;
  workflowId?: string;
  context?: IRequestContext[];
}

interface IRequestContext {
  type?: string;
  label?: string;
  comment?: string;
  hash?: string;
  status?: string;
  owner?: string;
  requestType?: string;
}

interface IReferencePoint {
  id: number;
  label: string;
  referencePoint: {
    type: string;
    typeName: string;
    coordinates: any[];
  };
}

interface IGeoCoordinates {
  id: number;
  label: string;
  geoCoordinates: IGeo;
}

interface IGeo {
  type: string;
  typeName: string;
  coordinates: any[];
}
interface ICurrentLocation {
  latitude?: number;
  longitude?: number;
}
interface IParcel {
  polygon: any;
  label: string;
  marker: any;
  referencePoint: any;
  scheme: string;
  type: string;
}
