import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core'
const { CameraPreview } = Plugins;
import { CameraPreviewOptions, CameraPreviewPictureOptions } from '@capacitor-community/camera-preview';
import { PhotoService } from './photo.service'
import { Router } from '@angular/router';
import { LocationService } from './location.service';
@Injectable({
  providedIn: 'root'
})
export class ArCameraService {
  cameraActive = false;

  constructor(
    public photoService: PhotoService,
    private go: Router,
    private locationService: LocationService
  ) { }

  openCamera() {
    const cameraPreviewOptions: CameraPreviewOptions = {
      position: 'rear',
      parent: 'cameraPreview',
      className: 'cameraPreview',
      toBack: true
    };
    CameraPreview.start(cameraPreviewOptions);
    this.cameraActive = true;
  }

  async stopCamera() {
    await CameraPreview.stop();
    this.cameraActive = false;
  }

  async captureImage(requestId) {
    const cameraPreviewPictureOptions: CameraPreviewPictureOptions = {
      quality: 90
    };
    const result = await CameraPreview.capture(cameraPreviewPictureOptions);
    await this.photoService.addNewToGallery(result);
    this.stopCamera();
    this.cameraActive = false;
    this.go.navigate(['/tabs/gallery/' + requestId]);
  }
}
