import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Plugins } from '@capacitor/core';
const { Network } = Plugins;

@Injectable({
  providedIn: 'root'
})

export class NetworkService {
  isOnline: Observable<boolean>;
  private statusSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private toastController: ToastController,
    private translate: TranslateService, private zone: NgZone) {
      Network.getStatus().then(status => this.statusSubject.next(status.connected));
      this.isOnline = this.statusSubject.asObservable();
      Network.addListener('networkStatusChange', (status) => {
        this.zone.run(() => {
          this.statusSubject.next(status.connected);
          this.updateNetworkStatus(status.connected);
        });
      });
  }

  private async updateNetworkStatus(status: boolean) {
      let toast;
      // tslint:disable-next-line: deprecation
      this.translate.get('Network.Toast').subscribe(
        value => {
          if (status) {
            toast = this.toastController.create({
              message: value + 'Online',
              duration: 3000,
              position: 'top',
              cssClass: 'custom-toast',
              buttons: [
                {
                  side: 'start',
                  icon: 'cloud-done-sharp',
                  handler: () => {
                    console.log('');
                  }
                }
              ]
            });
          }
          else {
            toast = this.toastController.create({
              message: value + 'Offline',
              duration: 3000,
              position: 'top',
              cssClass: 'custom-toast-warning',
              buttons: [
                {
                  side: 'start',
                  icon: 'cloud-offline-sharp',
                  handler: () => {
                    console.log('');
                  }
                }
              ]
            });
          }
  
        }
      )
      // tslint:disable-next-line: no-shadowed-variable
      toast.then(toast => toast.present());
    
  }

  public onNetworkChange(): Observable<boolean> {
    return this.statusSubject.asObservable();
  }

  public getCurrentNetworkStatus(): boolean {
    console.log('current network status: ', this.statusSubject.getValue())
    return this.statusSubject.getValue();
  }
}
