import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { SendService } from './send.service';
import { StorageService } from './storage.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from './authentication.service';
import { storedReq } from 'config';
import { Router } from '@angular/router';
import { ApiService } from './api.service';
import { PhotoService } from './photo.service';
@Injectable({
  providedIn: 'root',
})
export class OfflineManagerService {
  constructor(
    private sendService: SendService,
    private storageService: StorageService,
    private toastController: ToastController,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private go: Router,
    private sendAPI: ApiService,
    private photoService: PhotoService
  ) {}

  async checkForEvents(): Promise<void> {
    if (this.storageService.getLocalData(storedReq + this.auth.currentToken) !== null) {
      this.storageService.getLocalData(storedReq + this.auth.currentToken).then((response) => {
        if (response) {
          const responseArray = JSON.parse(response);
          responseArray.forEach((element) => {
            this.sendService
              .getSingleFile(element.file)
              .then((fileBlob) => {
                this.sendAPI.postResponse(
                    element.hash,
                    element.name,
                    element.data,
                    fileBlob
                  )
                  .then(async (sendResponse) => {
                    console.log(
                      'server response',
                      JSON.stringify(sendResponse)
                    );
                    if (sendResponse.status === 200) {
                      let toast;
                      // tslint:disable-next-line: deprecation
                      this.translate.get('Offline.Toast').subscribe((value) => {
                        toast = this.toastController.create({
                          message: value,
                          duration: 3000,
                          position: 'top',
                          cssClass: 'custom-toast',
                          buttons: [
                            {
                              side: 'start',
                              icon: 'checkmark-outline',
                              handler: () => {
                                console.log('');
                              },
                            },
                          ],
                        });
                      });
                      // tslint:disable-next-line: no-shadowed-variable
                      toast.then((toast) => toast.present());
                      this.storageService.removeLocalData(storedReq + this.auth.currentToken);
                      this.photoService.markAsSent(element.name);
                    }
                  })
                  .catch((error) => {
                    if (error.status === 401) {
                      let t;
                      this.translate
                        .get('Offline.Expired')
                        .subscribe((value) => {
                          t = this.toastController.create({
                            message: value,
                            position: 'top',
                            duration: 3000,
                            cssClass: 'custom-toast-warning',
                            buttons: [
                              {
                                side: 'start',
                                icon: 'alert-circle-outline',
                                handler: () => {
                                  console.log('');
                                },
                              },
                            ],
                          });
                        });
                      // tslint:disable-next-line: no-shadowed-variable
                      t.then((t) => t.present());
                      this.photoService.deselectPhotos();
                      this.go.navigate(['login']);
                    } else {
                      console.log('error repsonse', JSON.stringify(error));
                      let t;
                      this.translate.get('Offline.Error').subscribe((value) => {
                        t = this.toastController.create({
                          message: value,
                          position: 'top',
                          duration: 3000,
                          cssClass: 'custom-toast-warning',
                          buttons: [
                            {
                              side: 'start',
                              icon: 'alert-circle-outline',
                              handler: () => {
                                console.log('');
                              },
                            },
                          ],
                        });
                      });
                      // tslint:disable-next-line: no-shadowed-variable
                      t.then((t) => t.present());
                      this.photoService.deselectPhotos();
                    }
                  });
                  // this.storageService.removeLocalData(storedReq);
              })
              .catch((err) => {
                console.log('Error while reading file.');
              });
          });
        } else {
          console.log('no local events to sync');
        }
      });
    } else {
      console.log('no data found');
    }
  }
}
