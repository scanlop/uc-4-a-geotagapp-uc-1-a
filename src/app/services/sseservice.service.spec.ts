import { TestBed } from '@angular/core/testing';

import { SseService } from './sseservice.service';

describe('SseserviceService', () => {
  let service: SseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
