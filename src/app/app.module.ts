import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope/ngx';
import { DeviceOrientation, DeviceOrientationCompassHeading } from '@ionic-native/device-orientation/ngx';
// import { GoogleMaps } from '@ionic-native/google-maps';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, SafePipe } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { PhotoDetailsPageModule } from './pages/photo-details/photo-details.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { Device } from '@ionic-native/device/ngx';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import { File } from '@ionic-native/File/ngx';
import { IonicGestureConfig } from './utils/IonicGestureConfig';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Media } from '@ionic-native/media/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { PinchZoomModule } from 'ngx-pinch-zoom';

export function httpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/locale/', '.json');
}
/**
 *  Get Native Filereader and replace
 * Capacitor file reader buggy
 */
export class FileReaderA extends window.FileReader {
  constructor() {
    super();
    const zoneOriginalInstance = (this as any)['__zone_symbol__originalInstance'];
    return zoneOriginalInstance || this;
  }
}

window.FileReader = FileReaderA;
@NgModule({
  declarations: [AppComponent, SafePipe],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    PhotoDetailsPageModule,
    HttpClientModule,
    PinchZoomModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: (httpLoaderFactory),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Gyroscope,
    DeviceOrientation,
    AndroidPermissions,
    ScreenOrientation,
    HTTP,
    Network,
    Device,
    File,
    Media,
    Vibration,
    LocationAccuracy,
    Diagnostic,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
