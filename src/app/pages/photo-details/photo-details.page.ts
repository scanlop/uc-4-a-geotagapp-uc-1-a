import { Component, OnInit, Input } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-photo-details',
  templateUrl: './photo-details.page.html',
  styleUrls: ['./photo-details.page.scss'],
})
export class PhotoDetailsPage implements OnInit {
  @Input() latitude: string;
  @Input() longitude: string;
  @Input() direction: string;
  @Input() gyro_x: string;
  @Input() gyro_y: string;
  @Input() gyro_z: string;
  @Input() inLandParcel: string;
  @Input() distanceFromMarker: number;
  @Input() requestId: number;
  @Input() parcelId: string;
  @Input() isGal: boolean;
  @Input() isBei: boolean;
  @Input() isGlo: boolean;
  @Input() isGps: boolean;
  @Input() isQzss: boolean;
  @Input() isSbas: boolean;
  @Input() isDual: boolean;
  @Input() isOsnma: boolean;

  ios: boolean;
  android: boolean;

  constructor(private modalController: ModalController,
              private translate: TranslateService,
              private platform: Platform) {
                this.ios = platform.is('ios');
                this.android = platform.is('android');
               }

  ngOnInit() {
  }
  closeModal() {
    this.modalController.dismiss();
  }

}
