import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { httpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { SingleSchemePageRoutingModule } from './single-scheme-routing.module';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { SingleSchemePage } from './single-scheme.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingleSchemePageRoutingModule,
    IonBottomDrawerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [SingleSchemePage]
})
export class SingleSchemePageModule { }
