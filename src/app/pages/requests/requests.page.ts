import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { isPlatform, Platform, ToastController } from '@ionic/angular';
import { NetworkService } from '../../services/network.service';
import { TranslateService } from '@ngx-translate/core';
import { LocationService } from '../../services/location.service';
import { StorageService } from '../../services/storage.service';
import { ApiService } from 'src/app/services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AudioService } from 'src/app/services/audio.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { storedReq, TOKEN_KEY } from 'config';

@Component({
  selector: 'app-requests',
  templateUrl: 'requests.page.html',
  styleUrls: ['requests.page.scss'],
})
export class RequestsPage implements OnInit {
  private platform: Platform;
  myTitle = 'To AgriSnap';
  id: string;
  uc4array: any[] = [];
  private expired: string;
  private requestFailed: string;
  private coordinatedFailed: string;
  // notifications = [] as any[];
  constructor(
    private toastController: ToastController,
    private networkService: NetworkService,
    private storageService: StorageService,
    private requestAPI: ApiService,
    platform: Platform,
    private route: Router,
    private androidPermissions: AndroidPermissions,
    private translate: TranslateService,
    public locationService: LocationService,
    private loadingController: LoadingController,
    private auth: AuthenticationService,
    private audio: AudioService,
    private cd: ChangeDetectorRef,
    private diagnostic: Diagnostic
  ) {
    this.platform = platform;
  }
  /**
   *  on init
   * load android permissions
   */
  async ngOnInit() {
    await this.diagnostic.requestLocationAuthorization();
    if (isPlatform('android')) {
      await this.androidPermissions
        .checkPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE)
        .then(async (data: any) => {
          if (!data.hasPermission) {
            await this.androidPermissions.requestPermission(
              this.androidPermissions.PERMISSION.READ_PHONE_STATE
            );
          }
        });
    }
  }

  async ionViewWillEnter() {
    this.uc4array = [];
    // this.presentLoading();
    this.getTranslations();
    /* this.audio.getRequestAudioFile();
    this.audio.requestsAudioFile.onStatusUpdate.subscribe((status) => {
      if (status === 4) {
        // this.playing = false
        this.cd.detectChanges();
        this.audio.requestsAudioFile.release();
      }
    }); */
    this.readData();
  }

  async readData() {
    // check if offline , bug in network setting offline = online
    if (this.networkService.getCurrentNetworkStatus() === false) {
      let toast: Promise<HTMLIonToastElement>;
      // tslint:disable-next-line: deprecation
      this.translate.get('Notifications.Offline').subscribe((value) => {
        // value is our translated string
        toast = this.toastController.create({
          message: value,
          duration: 3000,
          position: 'top',
          cssClass: 'custom-toast',
          buttons: [
            {
              side: 'start',
              icon: 'cloud-offline-sharp',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
      });
      // tslint:disable-next-line: no-shadowed-variable
      toast.then((toast) => toast.present());
      await this.storageService.getLocalData(storedReq + this.auth.currentToken).then(async (res) => {
        this.requestAPI.UC4A = res;
        for (const context of this.requestAPI.UC4A.contexts) {
          this.uc4array.push(context);
        }
        this.loadingController.dismiss();
      });
      this.loadingController.dismiss();
    } else {
      let toast: Promise<HTMLIonToastElement>;
      this.storageService.getLocalData(TOKEN_KEY).then(storedToken => {
        if (storedToken !== null) {
          this.requestAPI.getRequests(storedToken).then(response => {
            if (response.status === 200) {
              const responseData : any = JSON.parse(response.data);
              const parseddata = JSON.parse(responseData.data);
              if (responseData.data === 'Error: User Not Found.') {
                this.translate.get('Login.Error').subscribe((value) => {
                  // value is our translated string
                  toast = this.toastController.create({
                    message: value,
                    duration: 3000,
                    position: 'top',
                    cssClass: 'custom-toast',
                    buttons: [
                      {
                        side: 'start',
                        icon: 'warning-sharp',
                        handler: () => {
                          console.log('');
                        },
                      },
                    ],
                  });
                });
                // tslint:disable-next-line: no-shadowed-variable
                toast.then((toast) => toast.present());
              } else {
                this.requestAPI.UC4A = parseddata;
                this.storageService.setLocalData(storedReq + this.auth.currentToken, parseddata);
                for (const context of this.requestAPI.UC4A.contexts) {
                  this.uc4array.push(context);
                }
              }
            } else {
              this.translate.get('Login.Error').subscribe((value) => {
                // value is our translated string
                toast = this.toastController.create({
                  message: value,
                  duration: 3000,
                  position: 'top',
                  cssClass: 'custom-toast',
                  buttons: [
                    {
                      side: 'start',
                      icon: 'warning-sharp',
                      handler: () => {
                        console.log('');
                      },
                    },
                  ],
                });
              });
              // tslint:disable-next-line: no-shadowed-variable
              toast.then((toast) => toast.present());
            }
          }).catch(error => {
            this.translate.get('Login.Failure').subscribe((value) => {
              // value is our translated string
              toast = this.toastController.create({
                message:  value + ' (' + error.status + ')',
                duration: 3000,
                position: 'top',
                cssClass: 'custom-toast',
                buttons: [
                  {
                    side: 'start',
                    icon: 'warning-sharp',
                    handler: () => {
                      console.log('');
                    },
                  },
                ],
              });
            });
            // tslint:disable-next-line: no-shadowed-variable
            toast.then((toast) => toast.present());
            console.log('scanlop error ', JSON.stringify(error));
          });
        }
      });
    }
  }

  ionViewDidLeave() {
    // this.audio.requestsAudioFile.release();
  }

  async presentLoading() {
    let text: string;
    this.translate.get('Wait.Wait').subscribe(value => {
      text = value;
    });
    const loading = await this.loadingController.create({
      message: text,
      translucent: true,
      duration: 5000
    });
    return await loading.present();
  }

  playAudio() {
    if (!this.audio.requestPlaying) {
      this.audio.requestPlaying = true;
      this.audio.requestsAudioFile.setVolume(1);
      this.audio.requestsAudioFile.play();
    } else {
      this.audio.requestPlaying = false;
      this.audio.requestsAudioFile.pause();
    }
  }

  doRefresh(refresher) {
    this.uc4array = [];
    // this.presentLoading();
    this.readData();
    setTimeout(() => {
      refresher.target.complete();
    }, 500);
  }

  /**
   * navigate to settings page
   */
  goToSettings() {
    this.route.navigate(['/settings']);
  }

  getTranslations() {
    this.translate.get('Notifications.Expired').subscribe((value) => {
      this.expired = value;
    });
    this.translate.get('Notifications.RequestsFailed').subscribe((value) => {
      this.requestFailed = value;
    });
    this.translate.get('Notifications.CoordinatesFailed').subscribe((value) => {
      this.coordinatedFailed = value;
    });
  }
}
