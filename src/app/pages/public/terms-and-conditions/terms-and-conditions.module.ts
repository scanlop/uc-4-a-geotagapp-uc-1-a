import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { httpLoaderFactory } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { IonicModule } from '@ionic/angular';
import { TermsAndConditionsPageRoutingModule } from './terms-and-conditions-routing.module';
import { TermsAndConditionsPage } from './terms-and-conditions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsAndConditionsPageRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    })

  ],
  declarations: [TermsAndConditionsPage]
})
export class TermsAndConditionsPageModule { }
