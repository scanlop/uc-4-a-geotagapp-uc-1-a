import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SsoLoginPageRoutingModule } from './sso-login-routing.module';

import { SsoLoginPage } from './sso-login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SsoLoginPageRoutingModule
  ],
  declarations: [SsoLoginPage]
})
export class SsoLoginPageModule {}
