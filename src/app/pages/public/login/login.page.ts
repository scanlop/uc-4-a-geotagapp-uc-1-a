import { Component, OnInit, ElementRef } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';
import { StorageService } from '../../../services/storage.service';
import { isPlatform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { acceptedTerms, storedReq } from 'config';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  accessToken = {
    digit1: '',
    digit2: '',
    digit3: '',
    digit4: '',
    digit5: ''
  }
  errorString: string = null;

  acceptTerms = {
    value: 'accepted',
    isChecked: false,
  }

  TermsValue = false;

  private error: string;
  private failure: string;

  constructor(
    private auth: AuthenticationService,
    private storageService: StorageService,
    private requestAPI: ApiService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.storageService.getLocalData('accepted').then(res => {
      if (res !== null) {
        this.acceptTerms = JSON.parse(res);
        this.TermsValue = this.acceptTerms.isChecked;
      }
    })
  }
  // NOT working on Android : revisit on next release
  moveToNext(event, num: number) {

    if (isPlatform('android')) {
      return;
    }

    if (num === 6) {
      return;
    }
    if (event.explicitOriginalTarget.value.length === 1) {

      let nextControl: any = document.getElementsByName('digit' + num);

      // console.log('test', nextControl);
      nextControl = nextControl[0];

      // Searching for next similar control to set it focus
      if (nextControl) {
        // console.log(nextControl.firstChild);
        nextControl.firstChild.focus();
        return;
      }
      else {
        nextControl = nextControl.nextElementSibling;
      }
    }
    else {
      return;
    }
  }

  /**
   * on checkbox change
   */
  checkTerms(event): void {
    this.TermsValue = event.detail.checked;
  }

  async SubmitToken() {
    const token = this.accessToken.digit1 + this.accessToken.digit2 +
      this.accessToken.digit3 + this.accessToken.digit4 + this.accessToken.digit5;
    const upperToken = token.toUpperCase();
    this.auth.authenticate(this.TermsValue);
    this.requestAPI.getRequests(upperToken).then(response => {
      if (response.status === 200) {
        const data : any = JSON.parse(response.data);
        const parseddata = JSON.parse(data.data);
        if (data.data === 'Error: User Not Found.') {
          this.translate.get('Login.Error').subscribe((value) => {
            this.errorString = value;
          });
        } else {
          this.auth.acceptTerms.isChecked = this.TermsValue;
          this.storageService.setLocalData(storedReq + this.auth.currentToken, parseddata);
          this.storageService.setLocalData(acceptedTerms, JSON.stringify(this.auth.acceptTerms))
          this.auth.login(upperToken);
        }
      } else {
        this.errorString = this.error
      }
    }).catch(error => {
      this.translate.get('Login.Failure').subscribe((value) => {
        this.errorString = value + '(' + error.status + ')';
      });
    });
    /* this.requestAPI.getRequests(upperToken).then(response => {
      if (response.status === 200) {
        const data : any = JSON.parse(response.data);
        const parseddata = JSON.parse(data.data);
        if (data.data === 'Error: User Not Found.') {
          this.errorString = 'That code is invalid, please try again'
        } else {
          console.log('scanlop requests ', JSON.stringify(parseddata))
          this.auth.authenticationState.next(true);
          this.requestAPI.UC4A = parseddata;
          this.acceptTerms.isChecked = this.TermsValue;
          this.storageService.setLocalData('requests', parseddata);
          // this.storageService.setLocalData('login-token', upperToken);
          this.storageService.setLocalData('accepted', JSON.stringify(this.acceptTerms));
        }
      } else {
        this.errorString = 'That code is invalid, please try again'
      }
    }).catch(error => {
      console.log('scanlop error ', JSON.stringify(error));
      this.errorString = 'That was an error, please try again (' + error.status + ')';
    }); */
  }
}


