import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { httpLoaderFactory } from '../../app.module';
import { HttpClient } from '@angular/common/http';
import { ParcelMapPageRoutingModule } from './parcel-map-routing.module';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { ParcelMapPage } from './parcel-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParcelMapPageRoutingModule,
    IonBottomDrawerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [ParcelMapPage]
})
export class ParcelMapPageModule { }
