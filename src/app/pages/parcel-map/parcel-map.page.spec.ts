import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParcelMapPage } from './parcel-map.page';

describe('ParcelMapPage', () => {
  let component: ParcelMapPage;
  let fixture: ComponentFixture<ParcelMapPage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ParcelMapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParcelMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
