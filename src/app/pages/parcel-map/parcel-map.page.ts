import { Component, OnInit, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { LocationService } from '../../services/location.service';
import { GeolocationPosition } from '@capacitor/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, IonBackButtonDelegate, LoadingController, Platform, ToastController } from '@ionic/angular';
import * as Leaflet from 'leaflet';
import { TranslateService } from '@ngx-translate/core';
import * as LeafletOffline from 'leaflet.offline';
import { StorageService } from '../../services/storage.service';
import { isPlatform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ArCameraService } from '../../services/ar-camera.service'
import { storedReq, urlTemplate } from 'config';
import { NetworkService } from 'src/app/services/network.service';
import { AudioService } from 'src/app/services/audio.service';
import { Vibration } from '@ionic-native/vibration/ngx';
import { PhotoService } from 'src/app/services/photo.service';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';

interface Notification {
  hash?: string;
  label: string;
  comment?: string;
  distance?: number;
  location?: string;
  coords?: any[];
  point?: boolean;
  referencePoint?: any[];
  paymentScheme?: string;
  producerCode?: string,
}

@Component({
  selector: 'app-parcel-map',
  templateUrl: './parcel-map.page.html',
  styleUrls: ['./parcel-map.page.scss'],
})
export class ParcelMapPage implements OnInit {

  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;
  constructor(
    private toastController: ToastController,
    private route: ActivatedRoute,
    private go: Router,
    public locationService: LocationService,
    public alertController: AlertController,
    private translate: TranslateService,
    private storage: StorageService,
    private locationAPI: ApiService,
    private networkService: NetworkService,
    private loadingController: LoadingController,
    private auth: AuthenticationService,
    public arCameraService: ArCameraService,
    private audio: AudioService,
    private cd: ChangeDetectorRef,
    private vibration: Vibration,
    private photoService: PhotoService,
    private locationAccuracy: LocationAccuracy,
    private diagnostic: Diagnostic,
    private geolocation: Geolocation,
    private platform: Platform
  ) {

    this.ios = platform.is('ios');
    this.android = platform.is('android');
    // start watching
    this.locationService.watchLocation();
  }

  ios: boolean;
  android: boolean;
  map: Leaflet.Map;
  private tilesDb: any;
  private osmTileLayer;
  private satelliteTileLayer;
  private watchingDevice;

  @ViewChild('map', { static: false })
  public mapElement: ElementRef;
  playing = false;
  longitude: number;
  latitude: number;
  notification = {} as Notification;
  pageTitle: string;
  deviceLocation = {} as any;
  // google maps marker
  devicePostion: any;
  isInField: number;
  closeEnough: number;
  landParcel: any;
  storedTiles = '';
  inParcel: string;
  closePoint: string;
  saved: string;
  confirmRemoval: string;
  removed: string;
  none: string;
  save: string;
  confirmSaved: string;
  saveExtended: string;
  confirmSavedExtended: string;
  locationUpdated: string;
  expired: string;
  coordinatesFailed: string
  request: boolean;
  deviceMarker = Leaflet.icon({
    iconUrl: './assets/images/location.svg',
    iconAnchor: [25, 25]
  })
  greenMarker = Leaflet.icon({
    iconUrl: './assets/images/green-camera-30.png',
    iconAnchor: [15, 15]
  })
  orangeMarker = Leaflet.icon({
    iconUrl: './assets/images/orange-camera-30.png',
    iconAnchor: [15, 15]
  })
  pins = Leaflet.layerGroup();
  baseLayer: any;
  alertHeader: string;
  alertSubHeader: string;
  alertButton: string;
  permission: boolean;

  /**
   * get notification data
   * get current location
   */
  async ngOnInit() {
    this.permission = true;
    // this.presentLoading();
    this.request = false;
    // get id from routing param
    if (this.locationAPI.random === this.route.snapshot.paramMap.get('id')) {
      this.pageTitle = this.locationAPI.selectedParcel.label;
      this.request = true;
    } else {
      const notification = this.route.snapshot.queryParams;
      // set page title based on query params
      this.pageTitle = notification.name;
    }
  }

  async ionViewWillEnter() {
    this.getInitialData();
    this.diagnostic.isLocationAvailable().then(async data => {
      if (data) {
        await this.watchDeviceLocation();
      } else {
        if (isPlatform('android')) {
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(async location => {
            if (location.code === 0 || location.code === 1) {
              this.diagnostic.isLocationAvailable().then(async response => {
                if (response) {
                  this.permission = true;
                  await this.watchDeviceLocation();
                }
              });
            } else {
              this.permission = false;
            }
          }).catch(async error => {
            this.loadingController.dismiss();
            this.permission = false;
            const alert = await this.alertController.create({
              header: this.alertHeader,
              subHeader: this.alertSubHeader,
              buttons: [this.alertButton]
            });
            alert.present();
          });
        }
        this.permission = false;
        // this.loadingController.dismiss();
      }
    });

    if (isPlatform('android')) {
      this.locationService.startGnss();
    }

    this.photoService.loadSaved();
    /*  if (this.request) {
       this.getData();
     } else {
       await this.getInitialData();
     }
     this.audio.requestsAudioFile.release();
 
     if (isPlatform('android')) {
       this.locationService.startGnss();
     }
     this.photoService.loadSaved();
     this.playing = false;
     this.audio.getLandParcelPageAudioFile();
     this.audio.getBeepAudioFile();
     this.audio.landParcelPageAudioFile.onStatusUpdate.subscribe(status => {
       if (status === 4) {
         this.playing = false;
         this.cd.detectChanges();
         this.audio.landParcelPageAudioFile.release();
       }
     });
     this.audio.beepAudioFile.onStatusUpdate.subscribe(status => {
       if (status === 4) {
         this.playing = false;
         this.cd.detectChanges();
         this.audio.beepAudioFile.release();
       }
     }); */
  }

  ionViewDidEnter() {
    this.backbuttonAction();
    // this.watchDeviceLocation();
  }

  playAudio() {
    if (!this.playing) {
      this.playing = true;
      this.audio.landParcelPageAudioFile.setVolume(1);
      this.audio.landParcelPageAudioFile.play();
    } else {
      this.playing = false;
      this.audio.landParcelPageAudioFile.pause();
    }
  }

  playBeep() {
    if (!this.playing) {
      this.playing = true;
      this.audio.beepAudioFile.setVolume(1);
      this.audio.beepAudioFile.play();
    } else {
      this.playing = false;
      this.audio.beepAudioFile.pause();
    }
  }

  backbuttonAction() {
    this.backButton.onClick = () => {
      this.audio.requestPlaying = false;
      this.locationService.stopGnss();
      this.locationService.stopWatch();
      this.go.navigate(['tabs/requests']);
    }
  }

  async presentLoading() {
    let text: string;
    this.translate.get('Wait.Wait').subscribe(value => {
      text = value;
    });
    const loading = await this.loadingController.create({
      message: text,
      translucent: true,
    });
    return await loading.present();
  }

  /* getData() {
    this.getTranslations();
    this.notification.label = this.locationAPI.selectedParcel.label;
    this.notification.referencePoint = [this.locationAPI.selectedParcel.referencePoint.lng,
    this.locationAPI.selectedParcel.referencePoint.lat];
    this.notification.scheme = this.locationAPI.selectedParcel.scheme;
    this.notification.type = this.locationAPI.selectedParcel.type;
    this.landParcel = this.locationAPI.selectedParcel.polygon;
    this.loadMap();
    this.loadingController.dismiss();
  }
 */
  async getInitialData() {
    this.getTranslations();
    const notificationId = this.route.snapshot.paramMap.get('id');
    let selectedNotification = [] as any;
    if (this.networkService.getCurrentNetworkStatus() === false) {
      await this.storage.getLocalData(storedReq + this.auth.currentToken).then(req => {
        this.locationAPI.UC4A = req;
      });
      const notifications = this.locationAPI.UC4A;
      for (const producer of notifications.contexts) {
        if (notificationId === producer.hash) {
          selectedNotification = producer;
          this.setData(selectedNotification);
          this.loadMap();
        } else {
          selectedNotification = null;
        }
      }
    } else {
      const notifications = this.locationAPI.UC4A;
      for (const producer of notifications.contexts) {
        if (notificationId === producer.hash) {
          selectedNotification = producer;
          this.setData(selectedNotification);
          this.loadMap();
        } else {
          selectedNotification = null;
        }
      }
    }
    /* await this.locationAPI
      .getGeometry(selectedNotification[0].label, this.auth.currentToken)
      .then((response) => {
        if (response.status === 200) {
          this.locationAPI.geoCoordinates = JSON.parse(response.data);
          this.getReferencePoint(selectedNotification[0].label);
        }
      })
      .catch((error) => {
        if (error.status === 401) {
          const t = this.toastController.create({
            message: 'Token expired please login again',
            position: 'top',
            duration: 3000,
            cssClass: 'custom-toast-warning',
            buttons: [
              {
                side: 'start',
                icon: 'alert-circle-outline',
                handler: () => {
                  console.log('');
                },
              ],
            });
            // tslint:disable-next-line: no-shadowed-variable
            t.then((t) => t.present());
            this.loadingController.dismiss();
            this.go.navigate(['login'])
            this.locationService.stopGnss();
            this.locationService.stopWatch();
          } else {
            console.log('DAFM error response ', JSON.stringify(error));
            const t = this.toastController.create({
              message: this.coordinatesFailed,
              position: 'top',
              duration: 3000,
              cssClass: 'custom-toast-warning',
              buttons: [
                {
                  side: 'start',
                  icon: 'alert-circle-outline',
                  handler: () => {
                    console.log('');
                  },
                },
              ],
            });
            // tslint:disable-next-line: no-shadowed-variable
            t.then((t) => t.present());
            this.loadingController.dismiss();
          }
        });
    }
    console.log('selected ', JSON.stringify(selectedNotification))
    // return selected notification
    this.setData(selectedNotification);
    // load Map
    this.loadMap();
    /* this.storage.getLocalData('requests')
      .then(res => {
        const requests = JSON.parse(res);
        const notifications = requests.context;
        const selectedNotification = notifications.filter(n => {
          return n.hash === notificationId;
        });
        // return selected notification
        this.setData(selectedNotification);
        // load Map
        this.loadMap();
      }
      )
      .catch(err => {
        console.log('error from getting local data', err);
      }); */
  }

  /**
   * Haversine Formula
   * Calculate distance between 2 points
   * @param lat1 number
   * @param lat2 number
   * @param lng1 number
   * @param lng2 number
   */
  HaversineFormula(lat1: number, lat2: number, lng1: number, lng2: number) {
    const R = 6371e3; // metres
    const φ1 = lat1 * Math.PI / 180; // φ, λ in radians
    const φ2 = lat2 * Math.PI / 180;
    const Δφ = (lat2 - lat1) * Math.PI / 180;
    const Δλ = (lng2 - lng1) * Math.PI / 180;

    const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) *
      Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c; // in metres
  }
  /**
   * Watch Device Location
   */
  watchDeviceLocation(bool?: boolean) {
    this.isInField = 0;
    // subscribe to subject
    // tslint:disable-next-line: deprecation
    this.locationService.watchLocationSub = this.geolocation
      .watchPosition({
        enableHighAccuracy: true,
        timeout: 20000,
        // tslint:disable-next-line: deprecation
      }).subscribe(
        (position) => {
          this.deviceLocation = position;
          const currentPosition = new Leaflet.LatLng(this.deviceLocation.coords.latitude, this.deviceLocation.coords.longitude);
          // load the device marker
          this.loadDeviceMarker();
          if (bool) {
            const t = this.toastController.create({
              message: this.locationUpdated,
              position: 'top',
              duration: 3000,
              cssClass: 'custom-toast',
              buttons: [
                {
                  side: 'start',
                  icon: 'compass-outline',
                  handler: () => {
                    console.log('');
                  },
                },
              ],
            });
            // tslint:disable-next-line: no-shadowed-variable
            t.then((t) => t.present());
            bool = false;
          }
          if (!this.notification.point) {
            this.notification.distance = this.HaversineFormula(this.deviceLocation.coords.latitude,
              this.latitude, this.deviceLocation.coords.longitude, this.longitude);
            // let contain = false;
            this.locationService.contain = this.landParcel.getBounds().contains(currentPosition);
            if (this.locationService.contain) {
              this.isInField = this.isInField + 1;
              if (this.isInField === 1) {
                this.vibration.vibrate(2000);
                this.playBeep();
                // ('I am in the parcel');
                this.presentToast('parcel');
              } else {
                return;
              }
            } else {
              this.isInField = 0;
            }
          } else {
            const marker = new Leaflet.LatLng(this.latitude, this.longitude);

            const distance = this.HaversineFormula(this.deviceLocation.coords.latitude,
              this.latitude, this.deviceLocation.coords.longitude, this.longitude);

            this.notification.distance = Math.round(distance);
            if (distance <= 10) {
              this.closeEnough = this.closeEnough + 1;
              if (this.closeEnough === 1) {
                this.vibration.vibrate(2000);
                this.playBeep();
                this.presentToast('point');
              }
            } else {
              this.closeEnough = 0;
            }
          }

        },
        (error) => {
          console.log('this is a GPS ERROR', error);
          this.watchDeviceLocation();
        }
      );
  }

  /* private getReferencePoint(landParcelLabel: string) {
    this.locationAPI.getReferencePoint(landParcelLabel, this.auth.currentToken).then(response => {
      if (response.status === 200) {
        const parsed = JSON.parse(response.data);
        this.locationAPI.referencePoint = parsed;
        this.notification.referencePoint = this.locationAPI.referencePoint.referencePoint.coordinates;
        // this.loadingController.dismiss();
      }
    }).catch((error) => {
      if (error.status === 401) {
        const t = this.toastController.create({
          message: this.expired,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
        this.go.navigate(['login'])
        this.locationService.stopGnss();
        this.locationService.stopWatch();
      } else {
        console.log('DAFM error response ', JSON.stringify(error));
        const t = this.toastController.create({
          message: this.coordinatesFailed,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
      }
    });
  } */
  /**
   * Assign data to a class field
   * @param data from json
   */
  async setData(data) {
    const type = data.geocoordinates.substr(0, data.geocoordinates.indexOf(' ('));
    const coords = data.geocoordinates.substr(data.geocoordinates.indexOf('(((') + 3, data.geocoordinates.lastIndexOf(')'));
    const sub = coords.substr(0, coords.indexOf(')'));
    let arrayCoords: any[] = [];
    arrayCoords = sub.split(',').map(value => {
      return value.trim();
    });
    this.notification.label = data.label;
    this.notification.hash = data.hash;
    this.notification.comment = data.comment;
    this.notification.paymentScheme = data.paymentScheme;
    this.notification.producerCode = data.producerCode;
    // console.log('scanlop type ', this.locationAPI.geoCoordinates.geoCoordinates.type);
    const geoCoordinates = arrayCoords;
    // console.log('scanlop geo ', type)
    if (type === 'Point') {
      // this.notification.point = true;
      // this.notification.location = data[0].geoCoordinates.coordinates;
    } else {
      const coordsArray: any[] = [];
      for (const c of geoCoordinates) {
        const coord = c.split(' ');
        const lat = coord[1];
        const lng = coord[0];
        coordsArray.push({ lng, lat });
      }
      this.notification.coords = coordsArray;
      this.landParcel = new Leaflet.Polygon(this.notification.coords);
    }
  }

  /**
   * once view is loaded
   * run OSM Maps with leaflet
   */
  async loadMap() {
    if (!this.notification.point) {
      const centre = this.landParcel.getBounds().getCenter();
      // const location = this.notification.location.split(',');
      this.latitude = centre.lat;
      this.longitude = centre.lng;
    } else {
      this.latitude = Number(this.notification.location[1]);
      this.longitude = Number(this.notification.location[0]);
    }
    const t = this;

    this.baseLayer = Leaflet.tileLayer.offline(urlTemplate, {
      minZoom: 15,
      maxZoom: 19,
      attribution: 'i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community <a href="http://www.esri.com/">Esri</a>',
    })

    // load map
    this.map = Leaflet.map('map', {
      center: [this.latitude, this.longitude],
      zoom: 16,
      layers: [this.baseLayer]
    });

    // add save and delete for offline storage
    const offlineControl = Leaflet.control.savetiles(this.baseLayer, {
      position: 'topright',
      zoomlevels: [13, 16], // optional zoomlevels to save, default current zoomlevel
      confirm(layer, succescallback) {
        // eslint-disable-next-line no-alert
        const tilesAmount: number = layer._tilesforSave.length;
        if (window.confirm(t.save + tilesAmount + t.saveExtended)) {

          succescallback();
          const toast = t.toastController.create({
            message: t.confirmSaved + tilesAmount + t.confirmSavedExtended,
            duration: 3000,
            position: 'top'
          });
          // tslint:disable-next-line: no-shadowed-variable
          toast.then(toast => toast.present());
        }
      },

      confirmRemoval(layer, successCallback) {
        let toast: Promise<HTMLIonToastElement>;
        // eslint-disable-next-line no-alert
        if (layer.lengthSaved) {
          if (window.confirm(t.confirmRemoval)) {
            successCallback();
            // console.log(layer);
            toast = t.toastController.create({
              message: t.removed,
              duration: 3000,
              position: 'top'
            });
          }
        }
        else {
          toast = t.toastController.create({
            message: t.none,
            duration: 3000,
            position: 'top'
          });
        }
        // tslint:disable-next-line: no-shadowed-variable
        toast.then(toast => toast.present());
      },

      saveText: '<div class="leaflet-save-button" style="width:30px;height:30px;line-height:40px;font-size:1.8em; background: #004D44; color: white;"><ion-icon name="cloud-download-sharp"></ion-icon></div>',
      rmText: '<div class="leaflet-save-button" style="width:30px;height:30px;line-height:40px;font-size:1.8em; background: #f48024; color: white;"><ion-icon name="trash-sharp"></ion-icon></div>',
    });
    offlineControl.addTo(this.map);

    // get geoJSON
    const getGeoJsonData = () => LeafletOffline.getStorageInfo(urlTemplate)
      .then((data) => LeafletOffline.getStoredTilesAsJson(this.baseLayer, data));


    // set defaults for icons , fix image bug
    const icon = './assets/images/marker-icon-blue.png';
    const iconShadow = './assets/images/marker-shadow.png';
    const rIcon = './assets/images/marker-icon-2x-blue.png'
    const DefaultIcon = Leaflet.icon({
      iconUrl: icon,
      iconSize: [25, 41],
      iconRetinaUrl: rIcon,
      shadowUrl: iconShadow,
      iconAnchor: [12, 41],
      popupAnchor: [-1, -36],
    });
    Leaflet.Marker.prototype.options.icon = DefaultIcon;
    // center on the marker
    this.map.panTo(new Leaflet.LatLng(this.latitude, this.longitude));
    // create data for info popup
    const contentString = '<div id="content" style="color: #666;"><strong> Land Parcel: ' +
      this.notification.label +
      '</strong></div>';
    // add a marker
    Leaflet.marker([this.latitude, this.longitude]).addTo(this.map).bindPopup(contentString);

    // Construct the polygon.
    if (!this.notification.point) {
      this.landParcel.addTo(this.map);
    }

    for (const photo of this.photoService.photos) {


      if (photo.pictureDetails.parcelId === this.notification.label) {
        // const centre = coords.polygon.getBounds().getCenter();
        const date = new Date(photo.pictureDetails.timeStamp);
        // tslint:disable-next-line:no-shadowed-variable
        const contentString = '<div id="content" style="color: #666;"><p><strong> Date & Time: </strong>' +
          date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() +
          '  ' + date.toLocaleTimeString() + '</p></div><br /> <img src= "' + photo.webviewPath + '" alt="' + photo.pictureDetails.name + '" />';
        // add a marker
        // tslint:disable-next-line:no-shadowed-variable
        let icon: Leaflet.icon = null;
        if (photo.pictureDetails.sent === true) {
          icon = this.greenMarker
        }
        else {
          icon = this.orangeMarker
        }
        const marker = Leaflet.marker([photo.pictureDetails.latitude,
        photo.pictureDetails.longitude], { icon }).bindPopup(contentString);
        this.pins.addLayer(marker)
      }
      this.pins.addTo(this.map);
    }

    const overlay = {
      Images: this.pins
    }
    const baseLayers = {
      Satellite: this.baseLayer,
    }

    Leaflet.control.layers(baseLayers, overlay, { position: 'topleft' }).addTo(this.map)
  }
  /**
   *  get device location and create new Marker object
   */
  loadDeviceMarker(): void {
    // console.log('scanlop devicePostion ', JSON.stringify(this.devicePostion));
    // console.log('scanlop devicelocation ', JSON.stringify([this.deviceLocation.coords.latitude,
    //   this.deviceLocation.coords.longitude]));
    // initialise marker
    if (!this.devicePostion) {
      this.devicePostion = Leaflet.marker([this.deviceLocation.coords.latitude,
      this.deviceLocation.coords.longitude], { icon: this.deviceMarker });
      try {
        this.devicePostion.addTo(this.map);
        this.loadingController.dismiss();
      } catch (e) {
        if (this.permission) {
          this.go.navigate(['']);
        }
        // console.log ('scanlop error ', JSON.stringify(e.message))
      }
    } else {
      // update marker position
      // prevents multiple markers being created
      this.devicePostion.setLatLng([this.deviceLocation.coords.latitude, this.deviceLocation.coords.longitude]);
    }

  }

  /**
   * Go to Camera Page
   * @param id hash value
   */
  goToCamera(id: string) {
    this.locationService.photoDetails.distanceFromMarker = this.notification.distance;
    this.locationService.photoDetails.parcelId = this.notification.label;
    this.locationService.photoDetails.requestId = this.notification.hash;
    this.locationService.photoDetails.scheme = this.notification.paymentScheme;
    this.locationService.photoDetails.producerCode = this.notification.producerCode;
    // console.log(this.locationService.photoDetails.device);
    if (this.notification.point) {
      this.locationService.photoDetails.isInField = 'not required';
    } else {
      if (this.locationService.contain) {
        this.locationService.photoDetails.isInField = 'true';
      } else {
        this.locationService.photoDetails.isInField = 'false';
      }
    }
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(data => {
      this.go.navigate(['/ar-camera/' + id]);
    }).catch(async error => {
      const alert = await this.alertController.create({
        header: this.alertHeader,
        subHeader: this.alertSubHeader,
        buttons: [this.alertButton]
      });
      alert.present();
    });
  }

  async presentToast(type: string) {
    if (type === 'parcel') {

      const toast = this.toastController.create({
        message: this.inParcel + this.notification.label,
        duration: 3000,
        position: 'top',
        cssClass: 'custom-toast',
        buttons: [
          {
            side: 'start',
            icon: 'compass-outline',
            handler: () => {
              console.log('');
            }
          }
        ]
      });
      // tslint:disable-next-line: no-shadowed-variable
      toast.then(toast => toast.present());

    } else if (type === 'point') {
      const toast = this.toastController.create({
        message: this.closePoint + this.notification.label,
        duration: 3000,
        position: 'top',
        cssClass: 'custom-toast',
        buttons: [
          {
            side: 'start',
            icon: 'compass-outline',
            handler: () => {
              console.log('');
            }
          }
        ]
      });
      // tslint:disable-next-line: no-shadowed-variable
      toast.then(toast => toast.present());
    }
    else if (type === 'saved') {
      const toast = this.toastController.create({
        message: this.saved,
        duration: 3000,
        position: 'top',
        cssClass: 'custom-toast',
        buttons: [
          {
            side: 'start',
            icon: 'checkmark-outline',
            handler: () => {
              console.log('');
            }
          }
        ]
      });
      // tslint:disable-next-line: no-shadowed-variable
      toast.then(toast => toast.present());
    }
  }

  private getTranslations() {
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.InParcel').subscribe(
      value => {
        this.inParcel = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ClosePoint').subscribe(
      value => {
        this.closePoint = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.Saved').subscribe(
      value => {
        this.saved = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ConfirmRemoval').subscribe(
      value => {
        this.confirmRemoval = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.Removed').subscribe(
      value => {
        this.removed = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.None').subscribe(
      value => {
        this.none = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.Save').subscribe(
      value => {
        this.save = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ConfirmSaved').subscribe(
      value => {
        this.confirmSaved = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.SaveExtended').subscribe(
      value => {
        this.saveExtended = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ConfirmSavedExtended').subscribe(
      value => {
        this.confirmSavedExtended = value;
      }
    )

    this.translate.get('Map.UpdateLocation').subscribe(
      value => {
        this.locationUpdated = value;
      }
    )
    this.translate.get('Toast.Expired').subscribe(
      value => {
        this.expired = value;
      }
    )

    this.translate.get('Toast.CoordinatesFailed').subscribe(
      value => {
        this.coordinatesFailed = value;
      }
    )
    this.translate.get('Map.AlertHeader').subscribe(
      value => {
        this.alertHeader = value;
      }
    )
    this.translate.get('Map.AlertSubHeader').subscribe(
      value => {
        this.alertSubHeader = value;
      }
    )
    this.translate.get('Map.AlertButton').subscribe(
      value => {
        this.alertButton = value;
      }
    )
  }

  ionViewDidLeave(): void {
    this.audio.beepAudioFile.release();
    //stop all location services
    this.locationService.stopGnss();
    this.locationService.stopWatch();
    this.locationService.watchLocationSub.unsubscribe();
    //reset device position
    this.devicePostion = null;
    // remove map
    this.map.off();
    this.map.remove();
    this.map.stop();
  }

  openMapsApp() {
    if (isPlatform('android')) {
      // ‘geo://’+latitude+’, ‘+longitude+’?q=’+query,+’_system’
      window.location.href = 'google.navigation:q=' + this.latitude + ',' +
        this.longitude; // + "(" + 'location' + ")&z=15";
    } else {
      window.location.href = 'maps://?q=' + this.latitude + ',' +
        this.longitude;
    }
  }
}
