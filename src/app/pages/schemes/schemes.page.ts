import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SCHEMES } from 'schemes';
@Component({
  selector: 'app-schemes',
  templateUrl: './schemes.page.html',
  styleUrls: ['./schemes.page.scss'],
})
export class SchemesPage implements OnInit {

  types = [];
  constructor(private go: Router,
    private translate: TranslateService,) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.types = [];
    this.types = SCHEMES.schemes;
  }

  goToScheme(scheme){
    this.go.navigate(['/single-scheme/'+ scheme]);
  }
}
