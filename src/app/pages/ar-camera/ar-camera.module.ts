import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArCameraPageRoutingModule } from './ar-camera-routing.module';

import { ArCameraPage } from './ar-camera.page';
import { SafePipe } from 'src/app/app.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArCameraPageRoutingModule
  ],
  declarations: [ArCameraPage, SafePipe]
})
export class ArCameraPageModule {}
