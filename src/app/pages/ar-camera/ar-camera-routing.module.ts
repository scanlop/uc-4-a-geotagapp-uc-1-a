import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArCameraPage } from './ar-camera.page';

const routes: Routes = [
  {
    path: '',
    component: ArCameraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArCameraPageRoutingModule {}
