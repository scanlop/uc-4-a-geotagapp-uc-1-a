import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { lang } from 'config';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  lang: string;
  type: string;
  constructor(private authService: AuthenticationService,
              private translate: TranslateService,
              private storage: StorageService) { }

  ngOnInit() {
    this.type = this.authService.userType;
  }

  logout() {
    this.authService.logout();
  }

  changeLanguage() {
    this.storage.setLocalData(lang, this.lang)
    this.translate.use(this.lang);
  }
}
