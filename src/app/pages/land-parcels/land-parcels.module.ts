import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LandParcelsPage } from './land-parcels.page';
import { LandParcelCardComponent } from '../../components/land-parcel-card/land-parcel-card.component'
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { httpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forChild([{ path: '', component: LandParcelsPage }])
  ],
  declarations: [LandParcelsPage, LandParcelCardComponent]
})
export class LandParcelsPageModule { }
