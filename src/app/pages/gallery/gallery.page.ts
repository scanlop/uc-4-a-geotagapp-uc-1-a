import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { PhotoService } from '../../services/photo.service';
import { ArCameraService } from '../../services/ar-camera.service';
import { SendService } from '../../services/send.service';
import { ActionSheetController, AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { PhotoDetailsPage } from '../photo-details/photo-details.page';
import { TranslateService } from '@ngx-translate/core';
import { NetworkService } from '../../services/network.service';
import { StorageService } from '../../services/storage.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { id, storedReq } from '../../../../config'
import { AudioService } from 'src/app/services/audio.service';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-gallery',
  templateUrl: 'gallery.page.html',
  styleUrls: ['gallery.page.scss']
})
export class GalleryPage implements OnInit {

  constructor(public photoService: PhotoService,
    public arCameraService: ArCameraService,
    public actionSheetController: ActionSheetController,
    public sendService: SendService,
    private modalController: ModalController,
    private translate: TranslateService,
    private networkService: NetworkService,
    private storageService: StorageService,
    private toastController: ToastController,
    private auth: AuthenticationService,
    private route: ActivatedRoute,
    private audio: AudioService,
    private cd: ChangeDetectorRef,
    private loadingController: LoadingController,
    private sant: DomSanitizer,
    private alertController: AlertController
  ) { }

  // stored Requets
  storedReqs = [] as any;
  playing = false;

  header: string;
  select: string;
  delete: string;
  cancel: string;
  more: string;
  toast: string;
  executed: number;
  hashId: string;
  loading: any;
  wait: string;
  overlayHidden = true;
  webviewPath: any;
  parcel: string;
  dateTaken: any;
  status: any;
  base64: any;
  received: string;
  not: string;
  searchData: string;
  scheme: string;
  alertButtonYes: string;
  alertButtonNo: string;
  deleteMessage: string;

  ionViewWillEnter() {
    this.overlayHidden = true;
    this.photoService.deselectPhotos();
    this.getTranslations();
    this.executed = 0;
    this.playing = false;
    this.audio.getPhotosAudioFile();
    this.audio.photosAudioFile.onStatusUpdate.subscribe(status => {
      if (status === 4) {
        this.playing = false;
        this.cd.detectChanges();
        this.audio.photosAudioFile.release();
      }
    });
    this.photoService.loadSaved();
    this.hashId = this.route.snapshot.paramMap.get(id);
  }

  async ngOnInit() {
    // this.getTranslations();

    // save to local storage
    this.storageService.getLocalData(storedReq + this.auth.currentToken)
      .then(res => {
        if (res !== null) {
          this.storedReqs = JSON.parse(res);
        }
      }
      );
  }

  public hideOverlay() {
    this.photoService.deselectPhotos();
    this.overlayHidden = true;
  }

  openOverlay(photo) {
    this.webviewPath = photo.webviewPath;
    this.base64 = photo.base64;
    this.parcel = photo.pictureDetails.parcelId;
    this.status = photo.pictureDetails.sent;
    this.dateTaken = photo.pictureDetails.timeStamp;
    this.scheme = photo.pictureDetails.scheme;
    this.overlayHidden = false;
  }

  playAudio() {
    if (!this.playing) {
      this.playing = true;
      this.audio.photosAudioFile.setVolume(1);
      this.audio.photosAudioFile.play();
    } else {
      this.playing = false;
      this.audio.photosAudioFile.pause();
    }
  }

  public async showActionSheet(photo, position) {
    // console.log('photo details ', JSON.stringify(photo));
    const actionSheet = await this.actionSheetController.create({
      header: this.header,
      buttons: [{
        text: this.select,
        icon: 'eye-outline',
        role: 'select',
        handler: () => {
          this.photoService.deselectPhotos();
          this.photoService.selectPicture(photo, position);
          this.openOverlay(photo);
          this.executed = 0;
        }
      }, {
        text: this.delete,
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
          this.executed = 0;
        }
      }, {
        text: this.cancel,
        icon: 'close',
        role: 'deselect',
        handler: () => {
          this.photoService.cancelPicture(photo, position);
          this.executed = 0;
        }
      }, {
        text: this.more,
        role: 'select',
        icon: 'information-circle-outline',
        handler: () => {
          this.OpenModal(photo);
          this.executed = 0;
        }
      }]
    });
    this.executed = this.executed + 1;
    if (this.executed === 1) {
      await actionSheet.present();
    }
  }
  public selectPhoto(photo, position): void {
    this.photoService.selectPicture(photo, position);
  }

  private getTranslations() {
    // tslint:disable-next-line: deprecation
    this.translate.get('Gallery.Header').subscribe(
      value => {
        this.header = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Gallery.View').subscribe(
      value => {
        this.select = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Gallery.Delete').subscribe(
      value => {
        this.delete = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Gallery.Cancel').subscribe(
      value => {
        this.cancel = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Gallery.More').subscribe(
      value => {
        this.more = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Gallery.OfflineToast').subscribe(
      value => {
        this.toast = value;
      }
    )

    this.translate.get('Gallery.Received').subscribe(
      value => {
        this.received = value;
      }
    )

    this.translate.get('Gallery.Not').subscribe(
      value => {
        this.not = value;
      }
    )

    this.translate.get('Wait.Wait').subscribe(
      value => {
        this.wait = value;
      }
    )
    this.translate.get('Gallery.DeleteMessage').subscribe(
      value => {
        this.deleteMessage = value;
      }
    )
    this.translate.get('Gallery.AlertButtonYes').subscribe(
      value => {
        this.alertButtonYes = value;
      }
    )
    this.translate.get('Gallery.AlertButtonNo').subscribe(
      value => {
        this.alertButtonNo = value;
      }
    )
  }

  search(event) {
    if (!event) {
      this.photoService.galleryPhotos = [...this.photoService.photos];
    } else {
      this.photoService.galleryPhotos = this.photoService.photos.filter((text) => {
        const pipe = new DatePipe('en-US')
        const dateSlashdmy = pipe.transform(text.pictureDetails.timeStamp, 'dd/MM/yyyy');
        const dateSlashmdy = pipe.transform(text.pictureDetails.timeStamp, 'MM/dd/yyyy');
        const dateSlashymd = pipe.transform(text.pictureDetails.timeStamp, 'yyyy/MM/dd');
        const dateSlashydm = pipe.transform(text.pictureDetails.timeStamp, 'yyyy/dd/MM');
        const dateDashdmy = pipe.transform(text.pictureDetails.timeStamp, 'dd-MM-yyyy');
        const dateDashmdy = pipe.transform(text.pictureDetails.timeStamp, 'MM-dd-yyyy');
        const dateDashymd = pipe.transform(text.pictureDetails.timeStamp, 'yyyy-MM-dd');
        const dateDashydm = pipe.transform(text.pictureDetails.timeStamp, 'yyyy-dd-MM');
        const dateNonedmy = pipe.transform(text.pictureDetails.timeStamp, 'ddMMyyyy');
        const dateNonemdy = pipe.transform(text.pictureDetails.timeStamp, 'MMddyyyy');
        const dateNoneymd = pipe.transform(text.pictureDetails.timeStamp, 'yyyyMMdd');
        const dateNoneydm = pipe.transform(text.pictureDetails.timeStamp, 'yyyyddMM');

        return (text.pictureDetails.parcelId.toLowerCase().includes(event.toLowerCase()) ||
          dateSlashdmy.toLowerCase().includes(event.toLowerCase()) ||
          dateSlashmdy.toLowerCase().includes(event.toLowerCase()) ||
          dateSlashymd.toLowerCase().includes(event.toLowerCase()) ||
          dateSlashydm.toLowerCase().includes(event.toLowerCase()) ||
          dateDashdmy.toLowerCase().includes(event.toLowerCase()) ||
          dateDashmdy.toLowerCase().includes(event.toLowerCase()) ||
          dateDashymd.toLowerCase().includes(event.toLowerCase()) ||
          dateDashydm.toLowerCase().includes(event.toLowerCase()) ||
          dateNonedmy.toLowerCase().includes(event.toLowerCase()) ||
          dateNonemdy.toLowerCase().includes(event.toLowerCase()) ||
          dateNoneymd.toLowerCase().includes(event.toLowerCase()) ||
          dateNoneydm.toLowerCase().includes(event.toLowerCase())) ||
          text.pictureDetails.scheme.toLowerCase().includes(event.toLowerCase());

        /*         text.pictureDetails.timeStamp.getDate().toString().includes(event.toLowerCase()) ||
                text.pictureDetails.timeStamp.getMonth().toString().includes(event.toLowerCase()) ||
                text.pictureDetails.timeStamp.getFullYear().toString().includes(event.toLowerCase())); */
      });

      // this.photoService.photos = [...this.photoService.galleryPhotos];
    }
  }


  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: this.wait,
      translucent: true,
      duration: 4500
    });
    await this.loading.present();
  }

  /**
   * send Data to API
   */
  public async sendData() {
    // const selectedLength = this.photoService.selectedPhotos.length;
    // console.log('selected length ', selectedLength);
    this.presentLoading();
    for (const photo of this.photoService.selectedPhotos) {

      // set the image name
      const imageName = photo.pictureDetails.name;
      const photoData = JSON.stringify(photo.pictureDetails)

      // send the FILE to send service
      if (this.networkService.getCurrentNetworkStatus() === false) {
        // this.loading.dismiss();
        let savedData: StoredImage;
        savedData = { file: photo.filepath, hash: photo.pictureDetails.requestId, name: imageName, data: photoData };

        this.storedReqs.push(savedData);
        this.storageService.setLocalData(storedReq + this.auth.currentToken, JSON.stringify(this.storedReqs));
        // toast alert
        const toast = this.toastController.create({
          message: this.toast,
          duration: 3000,
          position: 'top',
          cssClass: 'custom-toast',
          buttons: [
            {
              side: 'start',
              icon: 'send',
              handler: () => {
                console.log('');
              }
            }
          ]

        });
        // tslint:disable-next-line: no-shadowed-variable
        toast.then(toast => toast.present());
        // deselect imagges
        this.photoService.deselectPhotos();
      }
      else {
        this.sendService.sendData(photo.pictureDetails.base64,
          photo.pictureDetails.requestId, imageName, photoData, this.auth.currentToken);

      }
    }
    if (!this.overlayHidden) {
      this.hideOverlay();
    }
  }

  public async deleteBatch() {
    const alert = await this.alertController.create({
      // header: 'Delete Selected Photos',
      subHeader: 'Some photos may not have been sent. Are you sure you want to delete them?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.photoService.batchDelete();
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log();
          }
        }
      ],
    });
    alert.present();
  }

  /**
   * OPen photo Modal
   * @ param photo
   */
  OpenModal(photo): void {
    this.modalController.create({
      component: PhotoDetailsPage,
      componentProps: {
        latitude: photo.pictureDetails.latitude,
        longitude: photo.pictureDetails.longitude,
        direction: photo.pictureDetails.direction,
        gyro_x: photo.pictureDetails.gyro_x,
        gyro_y: photo.pictureDetails.gyro_y,
        gyro_z: photo.pictureDetails.gyro_z,
        inLandParcel: photo.pictureDetails.isInField,
        distanceFromMarker: photo.pictureDetails.distanceFromMarker,
        requestId: photo.pictureDetails.requestId,
        parcelId: photo.pictureDetails.parcelId,
        isGal: photo.pictureDetails.isGal,
        isBei: photo.pictureDetails.isBei,
        isGlo: photo.pictureDetails.isGlo,
        isGps: photo.pictureDetails.isGps,
        isQzss: photo.pictureDetails.isQzss,
        isSbas: photo.pictureDetails.isSbas,
        isDual: photo.pictureDetails.isDual,
        isOsnma: photo.pictureDetails.isOsnma,
      }

    }).then((modalElement) => {
      modalElement.present();
    });
  }

  ionViewDidLeave() {
    this.audio.photosAudioFile.release();
    this.searchData = null;
    this.photoService.loadSaved();
  }
}

interface StoredImage {
  file: string,
  hash: string,
  name: string,
  data: string
}