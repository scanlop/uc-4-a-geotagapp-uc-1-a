import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'requests',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/requests/requests.module').then(m => m.RequestsPageModule)
          }
        ]
      },
      {
        path: 'gallery',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/gallery/gallery.module').then(m => m.GalleryPageModule)
          }
        ]
      },
      {
        path: 'gallery/:id',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/gallery/gallery.module').then(m => m.GalleryPageModule)
          }
        ]
      },
      {
        path: 'land-parcels',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/land-parcels/land-parcels.module').then(m => m.LandParcelsPageModule)
          }
        ]
      },
      {
        path: 'schemes',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/schemes/schemes.module').then( m => m.SchemesPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/requests',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/requests',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
