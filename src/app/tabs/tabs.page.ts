import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})


export class TabsPage implements OnInit {
  type: string;

  constructor(private translate: TranslateService, private auth: AuthenticationService) {

  }

  ngOnInit() {
    this.type = this.auth.userType;
  }

}
