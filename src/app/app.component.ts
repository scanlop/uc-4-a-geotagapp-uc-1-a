import { NetworkService } from './services/network.service';
import { OfflineManagerService } from './services/offline-manager.service';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { registerWebPlugin } from '@capacitor/core';
import { OAuth2Client } from '@byteowls/capacitor-oauth2';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './services/storage.service';
import { lang, TOKEN_KEY } from 'config';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private router: Router,
    private translate: TranslateService,
    private offlineManager: OfflineManagerService,
    private networkService: NetworkService,
    private storageService: StorageService,
    private screenOrientation: ScreenOrientation
  ) {
    // this.translate.setDefaultLang('en');
    this.initializeApp();
  }

  ngOnInit() {
    // console.log('Register custom capacitor plugins');
    registerWebPlugin(OAuth2Client);
  }

  initializeApp() {

    this.platform.ready().then(() => {
      // set to portrait
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      //set translate languages
      this.translate.addLangs(['en', 'it', 'fr', 'de', 'et', 'ga', 'gr']);
      this.storageService.getLocalData(lang).then((language) => {
        if (language === null || language === undefined) {
          this.translate.use('en');
        } else {
          this.translate.use(language);
        }
      })
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // first check if offline
      if (this.networkService.getCurrentNetworkStatus() === false) {
        this.storageService.getLocalData(TOKEN_KEY).then(storedToken => {
          if (storedToken !== null) {
            this.authenticationService.login(storedToken);
          }
        });
      }

      // tslint:disable-next-line: deprecation
      this.authenticationService.authenticationState.subscribe(state => {

        if (state) {
          this.router.navigate(['']);
        } else {
          this.router.navigate(['login']);
        }
      });
    });
    this.platform.backButton.subscribeWithPriority(10, () => {

      if (this.authenticationService.isAuthenticated) {
        this.router.navigate(['']);
      } else {
        this.router.navigate(['login']);
      }
    });

    // tslint:disable-next-line: deprecation
    this.networkService.onNetworkChange().subscribe((status: boolean) => {
      if (status) {
        this.offlineManager.checkForEvents();
      }
    });
  }
}

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
